import axios from "axios";
import { https, TOKEN_CYBER } from "./configURL";

export let userService = {
  postLogin: (loginData) => {
    return https.post("/api/QuanLyNguoiDung/DangNhap", loginData);
  },
  deleteUser: (taiKhoan) => {
    return https.delete(
      `api/QuanLyNguoiDung/XoaNguoiDung?TaiKhoan=${taiKhoan}`
    );
  },
};
