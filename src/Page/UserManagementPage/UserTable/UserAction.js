import { message } from "antd";
import React from "react";
import { userService } from "../../../services/user.service";

export default function UserAction({ taiKhoan, handleOnSuccess }) {
  let handleUserDelete = () => {
    userService
      .deleteUser(taiKhoan)
      .then((res) => {
        message.success("Xoá thành công");
        console.log(res);
        handleOnSuccess();
      })
      .catch((err) => {
        message.error(err.response.data.content);
        console.log(err);
      });
  };

  return (
    <div className="space-x-5">
      <button className="bg-blue-500 rounded text-white px-5 py-3">Sửa</button>
      <button
        className="bg-red-500 rounded text-white px-5 py-3"
        onClick={handleUserDelete}
      >
        Xoá
      </button>
    </div>
  );
}
