import { Space, Table, Tag } from "antd";
import React, { useEffect, useState } from "react";
import { movieService } from "../../../services/movie.service";
import { columns } from "../userManagement.utils";
import UserAction from "./UserAction";

const UserTable = () => {
  const [dataUser, setDataUser] = useState([]);
  useEffect(() => {
    let fetchListUser = () => {
      movieService
        .getUserList()
        .then((res) => {
          let userList = res.content.map((user) => {
            return {
              ...user,
              action: (
                <UserAction
                  onSuccess={fetchListUser}
                  taiKhoan={user.taiKhoan}
                />
              ),
            };
          });
          setDataUser(userList);
          console.log(res);
        })
        .catch((err) => {
          console.log(err);
        });
    };
    fetchListUser();
  }, []);

  return <Table columns={columns} dataSource={dataUser} />;
};

export default UserTable;
