import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { Switch } from "antd";
import LoginPage from "./Page/LoginPage/LoginPage";
import UserManagementPage from "./Page/UserManagementPage/UserManagementPage";
import NotFoundPage from "./Page/NotFoundPage/NotFoundPage";
import { adminRoute } from "./routes/adminRoute";
import Layout from "./Layout/Layout";

function App() {
  let renderRoutes = () => {
    return adminRoute.map(({ path, Component }) => {
      return (
        <Route
          path={path}
          element={
            <Layout>
              <Component />
            </Layout>
          }
        />
      );
    });
  };
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>{renderRoutes()}</Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
